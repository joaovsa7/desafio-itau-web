import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FeaturedHeaderComponent } from './modules/components/featured-header/featured-header.component';
import { GenericList } from './modules/components/generic-list/generic-list.component';
import { NavbarComponent } from './modules/components/navbar/navbar.component';
import { SelectComponent } from './modules/components/select/select.component';
import { FilterPipe } from './shared/pipe/filter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    FeaturedHeaderComponent,
    GenericList,
    NavbarComponent,
    SelectComponent,
    FilterPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
