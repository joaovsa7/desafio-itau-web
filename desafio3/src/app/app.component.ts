import { Component, OnInit } from '@angular/core';
import { BillsService } from './core/services/bills/bills-service';
import formatMonthsAndCategoriesName from './core/helpers/formatMonthsAndCategoriesName';
import BillsModel from './shared/models/bills/bills.model';
import formatMonthsNumberArray from './core/helpers/formatMonthsNumberArray';
import CategoryModel from './shared/models/category/category.model';
import { CategoryService } from './core/services/category/category.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'desafio3';
  categoriesItems: CategoryModel[];
  monthsSelectItems: Object[];
  billsList: BillsModel[];
  error: boolean;
  filterName: string;
  navTitle: string = 'Lançamentos';

  constructor(
    private billsService: BillsService,
    private categoriaService: CategoryService
  ) {}

  ngOnInit(): void {
    this.categoriaService.getAll().subscribe(
      (categories) => {
        this.categoriesItems = categories;
      },
      (err) => {
        console.log(err);
        this.error = true;
      }
    );

    this.billsService.getAll().subscribe(
      (data) => {
        this.billsList = formatMonthsAndCategoriesName(data);
        this.monthsSelectItems = formatMonthsNumberArray(data);
      },
      (err) => {
        console.log(err);
        this.error = true;
      }
    );
  }

  onChangeFilterName = (name: string) => {
    return (this.filterName = name);
  };

  toggleNavBarTitleAndCleanFilterName = (name: string) => {
    this.filterName = undefined;
    return (this.navTitle = name);
  };
}
