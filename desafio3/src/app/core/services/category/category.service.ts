import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import CategoryModel from '../../../shared/models/category/category.model';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  constructor(private http: HttpClient) {}
  private readonly API_URL = environment.API_URL;

  getAll(): Observable<Array<CategoryModel>> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
      }),
    };

    const endpoint: string = `${this.API_URL}/categorias`;

    return <Observable<Array<CategoryModel>>>(
      this.http.get(endpoint, httpOptions).pipe(take(1))
    );
  }
}
