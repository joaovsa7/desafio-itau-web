import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import LancamentoModel from '../../../shared/models/bills/bills.model';

@Injectable({
  providedIn: 'root',
})
export class BillsService {
  constructor(private http: HttpClient) {}
  private readonly API_URL = environment.API_URL;

  getAll(): Observable<Array<LancamentoModel>> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
      }),
    };

    const endpoint: string = `${this.API_URL}/lancamentos`;

    return <Observable<Array<LancamentoModel>>>(
      this.http.get(endpoint, httpOptions).pipe(take(1))
    );
  }
}
