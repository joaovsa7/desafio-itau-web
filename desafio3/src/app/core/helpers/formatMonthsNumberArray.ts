import { monthsMap } from 'src/app/shared/dataMap';

const formatMonthsNumberArray = (monthsArray: Object[]): Object[] => {
  const months = monthsArray.map(({ mes: month }: any): number => month);
  const cleanedArray: number[] = [...new Set(months)];

  return cleanedArray.map(
    (month): Object => {
      const monthName = monthsMap[month] || false;
      if (monthName) {
        return {
          name: monthName,
          id: month,
        };
      }
      return;
    }
  );
};

export default formatMonthsNumberArray;
