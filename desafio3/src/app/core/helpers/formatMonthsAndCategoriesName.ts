import BillsModel from 'src/app/shared/models/bills/bills.model';
import { monthsMap, categoriesMap } from 'src/app/shared/dataMap';

const formatMonthsAndCategoriesName = (billsList: Array<BillsModel>) => {
  if (!billsList.length) {
    return [];
  }
  return billsList.map((bill) => {
    const { mes: monthId } = bill;
    const formatedMonthName = monthsMap[monthId];
    const formatedCategoryName = categoriesMap[bill.categoria];
    return {
      ...bill,
      categoria: formatedCategoryName,
      mes: formatedMonthName,
    };
  });
};
export default formatMonthsAndCategoriesName;
