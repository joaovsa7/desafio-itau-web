import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericList } from './generic-list.component';

describe('GenericList', () => {
  let component: GenericList;
  let fixture: ComponentFixture<GenericList>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GenericList],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericList);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
