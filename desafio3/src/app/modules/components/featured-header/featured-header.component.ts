import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-featured-header',
  templateUrl: './featured-header.component.html',
  styleUrls: ['./featured-header.component.scss'],
})
export class FeaturedHeaderComponent {}
