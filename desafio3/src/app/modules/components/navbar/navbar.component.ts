import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent {
  @Input() onChange: Function;
  @Input() activeNavBar: string;
  navBarLinks: string[] = ['Lançamentos', 'Por mês', 'Por Categoria'];
}
