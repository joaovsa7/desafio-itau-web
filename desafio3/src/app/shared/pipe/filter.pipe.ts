import { PipeTransform, Pipe } from '@angular/core';
import BillsModel from 'src/app/shared/models/bills/bills.model';

@Pipe({
  name: 'filter',
})
export class FilterPipe implements PipeTransform {
  transform(billsList: BillsModel[], value: string) {
    if (!billsList.length || !value) {
      return billsList;
    }
    const filteredArray = billsList.filter(
      (item) => item.categoria === value || item.mes === value
    );
    return filteredArray;
  }
}
