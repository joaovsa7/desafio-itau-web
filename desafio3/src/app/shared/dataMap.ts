export const monthsMap = {
  '1': 'Janeiro',
  '2': 'Fevereiro',
  '3': 'Março',
  '4': 'Abril',
  '5': 'Maio',
  '6': 'Junho',
  '7': 'Julho',
  '8': 'Agosto',
  '9': 'Setembro',
  '10': 'Outubro',
  '11': 'Novembro',
  '12': 'Dezembro',
};

export const categoriesMap = {
  '1': 'Transporte',
  '2': 'Compras Online',
  '3': 'Saúde e Beleza',
  '4': 'Serviços Automotivos',
  '5': 'Restaurantes',
  '6': 'Super Mercados',
};
