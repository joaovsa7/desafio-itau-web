type BillsModel = {
  valor: number;
  origem: string;
  categoria: number | string;
  mes: number | string;
  id?: number;
  name?: string;
};

export default BillsModel;
