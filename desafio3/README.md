# Desafio3

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.6.

# Sobre o projeto

Projeto criado com a versão 9 do angular.

# Estruturas de pastas

- app
  - core
    - helpers
    - services
  - modules
    - components
  - shared
    - models
    - pipe
    - assets

### Projeto Hospedado:

[Desafio 3 Itau](https://desafio3itau.netlify.app/);
