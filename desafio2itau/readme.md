# DESAFIO 2

Não sou acostumado com o desenvolvimento em java e infelizmente neste desafio, não consegui concluir os testes unitários.

## ENDPOINTS

Retorna todos os lançamentos

- /lancamentos
  Retorna o lançamento baseado no id
  - /id
    Retorna os lançamentos baseado no id da categoria
  - /categoria/{id}

Retorna todas as categorias.
- /categorias
    Retorna uma categoria baseada no id.
    - /{id} 
    

#### PROJETO HOSPEDADO EM:

[DESAFIO 2](https://still-dawn-71901.herokuapp.com)
