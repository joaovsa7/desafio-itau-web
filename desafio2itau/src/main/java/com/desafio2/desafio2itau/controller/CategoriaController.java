package com.desafio2.desafio2itau.controller;

import com.desafio2.desafio2itau.modelo.Categoria;
import com.desafio2.desafio2itau.service.CategoriaService;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/categorias")
public class CategoriaController {
    private final CategoriaService categoriaService = new CategoriaService();

    @GetMapping("")
    public String getAll() {
        return categoriaService.getAll();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public List<Categoria> byId(@PathVariable String id) {
        return categoriaService.getById(id);
    }
}
