package com.desafio2.desafio2itau.controller;

import com.desafio2.desafio2itau.modelo.Lancamento;
import com.desafio2.desafio2itau.service.LancamentosService;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/lancamentos")
public class LancamentosController {
    private final LancamentosService lancamentosService = new LancamentosService();

    @GetMapping("")
    public List<Lancamento> getAll() {
        return lancamentosService.getAll();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public List<Lancamento> byId(@PathVariable String id) {
        return lancamentosService.getById(id);
    }

    @GetMapping("/categoria/{id}")
    @ResponseBody
    public List<Lancamento> byCategoriaId(@PathVariable String id) {
        return lancamentosService.getByCategoriaId(id);
    }

}