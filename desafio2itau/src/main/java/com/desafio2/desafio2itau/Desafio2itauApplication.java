package com.desafio2.desafio2itau;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Desafio2itauApplication {
	public static void main(String[] args) {
		SpringApplication.run(Desafio2itauApplication.class, args);
	}

}
