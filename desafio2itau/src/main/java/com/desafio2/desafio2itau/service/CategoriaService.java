package com.desafio2.desafio2itau.service;

import com.desafio2.desafio2itau.modelo.Categoria;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@EnableWebMvc
public class CategoriaService {
    final private RestTemplate restTemplate = new RestTemplate();
    final String URL = "https://desafio-it-server.herokuapp.com/categorias";
    final private HttpHeaders headers = new HttpHeaders();
    final private HttpEntity<String> entity = new HttpEntity<String>(headers);
    final private Gson gson = new Gson();

    public String getAll() {
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        try {
            return restTemplate.exchange(this.URL, HttpMethod.GET, entity, String.class).getBody();
        } catch(Error error){
            return error.toString();
        }
    }

    public List<Categoria> getById(String id){
        int categoriaId = Integer.parseInt(id);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        try {
            Type tipoDaLista = new TypeToken<List<Categoria>>(){}.getType();
            String response = restTemplate.exchange(this.URL, HttpMethod.GET, entity, String.class).getBody();
            List<Categoria> listaDeCategorias = gson.fromJson(response, tipoDaLista);
            return listaDeCategorias.stream().filter((categoria) -> categoria.getId() == categoriaId).collect(Collectors.toList());
        } catch(Error error){
             System.out.println(error);
             return null;
        }
    }
}
