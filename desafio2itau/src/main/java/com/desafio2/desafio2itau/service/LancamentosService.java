package com.desafio2.desafio2itau.service;

import com.desafio2.desafio2itau.modelo.Lancamento;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class LancamentosService {
    final private RestTemplate restTemplate = new RestTemplate();
    final private String URL = "https://desafio-it-server.herokuapp.com/lancamentos";
    final private HttpHeaders headers = new HttpHeaders();
    final private HttpEntity<String> entity = new HttpEntity<String>(headers);
    final private Gson gson = new Gson();

    public List<Lancamento> getAll() {
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        try {
            String bodyResponse = restTemplate.exchange(this.URL, HttpMethod.GET, entity, String.class).getBody();
            Type tipoDaLista = new TypeToken<List<Lancamento>>(){}.getType();
            List<Lancamento> listaDeLancamentos = gson.fromJson(bodyResponse, tipoDaLista);
            if(listaDeLancamentos == null){
                System.out.println("Houve um erro, tente novamente mais tarde");
                return null;
            }
            return listaDeLancamentos.stream().sorted(Comparator.comparing(Lancamento::getMes)).collect(Collectors.toList());
        } catch(Error error){
            System.out.println(error);
            return null;
        }
    }
    public List<Lancamento> getById(String id){
        int lancamentoId = Integer.parseInt(id);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        try {
            String bodyResponse = restTemplate.exchange(this.URL, HttpMethod.GET, entity, String.class).getBody();
            Type tipoDaLista = new TypeToken<List<Lancamento>>(){}.getType();
            List<Lancamento> listaDeLancamentos = gson.fromJson(bodyResponse, tipoDaLista);
            if(listaDeLancamentos == null){
                System.out.println("Houve um erro, tente novamente mais tarde");
                return null;
            }
            return listaDeLancamentos.stream().filter(lancamento -> lancamento.getId() == lancamentoId).collect(Collectors.toList());
        } catch(Error error){
            System.out.println(error);
            return null;
        }
    }

    public List<Lancamento> getByCategoriaId(String id){
        int categoriaId = Integer.parseInt(id);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        try {
            String bodyResponse = restTemplate.exchange(this.URL, HttpMethod.GET, entity, String.class).getBody();
            Type tipoDaLista = new TypeToken<List<Lancamento>>(){}.getType();
            List<Lancamento> listaDeLancamentos = gson.fromJson(bodyResponse, tipoDaLista);
            if(listaDeLancamentos == null){
                System.out.println("Houve um erro, tente novamente mais tarde");
                return null;
            }
            return listaDeLancamentos.stream().filter(lancamento -> lancamento.getCategoria() == categoriaId).collect(Collectors.toList());
        } catch(Error error){
            System.out.println(error);
            return null;
        }
    }
}
