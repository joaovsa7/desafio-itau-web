package br.com.itau;

import java.util.ArrayList;
import java.util.List;

import br.com.itau.modelo.Lancamento;
import br.com.itau.service.LancamentoService;

public class Exercicio {
	public static void main(String[] args) {
		List<Lancamento> lancamentos = new LancamentoService().listarLancamentos();
		getLancamentosOrdenadosPorMes(lancamentos);
		getLancamentosPorCategoria(lancamentos, 6);
		getTotalPorMes(lancamentos, 7);
	}

	static void getLancamentosPorCategoria(List<Lancamento> listaDeLancamentos, Integer categoria) {
		if (listaDeLancamentos.isEmpty()) {
			System.out.println("A lista está vazia.");
		}

		System.out.println("<----- Lançamentos ordenados por categoria ----->");
		listaDeLancamentos.stream().filter((lancamento) -> lancamento.getCategoria() == categoria)
				.forEach(lancamento -> System.out.println(lancamento.toString()));
	}

	static void getLancamentosOrdenadosPorMes(List<Lancamento> listaDeLancamentos) {
		if (listaDeLancamentos.isEmpty()) {
			System.out.println("A lista está vazia.");
		}
		System.out.println("<----- Lançamentos ordenados por mês ----->");
		listaDeLancamentos.stream().sorted((a, b) -> a.getMes().compareTo(b.getMes()))
				.forEach(lancamento -> System.out.println(lancamento.toString()));
	}

	static void getTotalPorMes(List<Lancamento> listaDeLancamentos, Integer mes) {
		if (listaDeLancamentos.isEmpty()) {
			System.out.println("A lista está vazia.");
		}

		List<Double> listaDeValores = new ArrayList<>();

		listaDeLancamentos.stream().forEach((lancamento) -> {
			if (lancamento.getMes() == mes) {
				listaDeValores.add(lancamento.getValor());
			}
		});

		if (listaDeValores.isEmpty()) {
			System.out.println("Não encontramos lançamentos no mês informado.");
		}

		String valorTotal = listaDeValores.stream()
				.reduce((double) 0, (lancamentoAtual, proxLancamento) -> lancamentoAtual + proxLancamento).toString();
		System.out.println("<----- Total da Fatura do mês" + mes + " ----->");
		System.out.println(valorTotal);
	}

}